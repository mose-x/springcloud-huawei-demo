package com.mose;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class HuaweiProvider8011Application {
    public static void main(String[] args) {
        SpringApplication.run(HuaweiProvider8011Application.class, args);
    }
}
