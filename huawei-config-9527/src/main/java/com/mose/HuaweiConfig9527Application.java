package com.mose;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class HuaweiConfig9527Application {
    public static void main(String[] args) {
        SpringApplication.run(HuaweiConfig9527Application.class, args);
    }
}
