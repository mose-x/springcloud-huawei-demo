package com.mose;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class HuaweiGateway9090Application {
    public static void main(String[] args) {
        SpringApplication.run(HuaweiGateway9090Application.class, args);
    }
}
