package com.mose;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClientSpecification;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
// 在8020工程中定义了该bean，所以此处不需要定义，若是单体项目（8021项目单独部署），则需要定义该bean
// @EnableFeignClients
public class HuaweiConsumer8021Application {
    public static void main(String[] args) {
        SpringApplication.run(HuaweiConsumer8021Application.class, args);
    }

// 在8020工程中定义了该bean，所以此处不需要定义，若是单体项目(8021项目单独部署)，则需要定义该bean
//    @LoadBalanced
//    @Bean(name = "RestTemplate2")
//    public RestTemplate restTemplate() {
//        return new RestTemplate();
//    }
}
