package com.mose;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Component
@FeignClient(value = "huawei-provider")
public interface ConsumerService {

    @RequestMapping(value = "/say", method = RequestMethod.GET)
    String say();

}
