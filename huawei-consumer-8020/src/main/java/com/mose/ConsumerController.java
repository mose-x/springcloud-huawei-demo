package com.mose;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
public class ConsumerController {
    @Resource
    private RestTemplate restTemplate;
    @Resource
    private ConsumerService consumerService;

    // 不推荐
    @GetMapping("/say")
    public String sayHello() {
        return restTemplate.getForObject("http://huawei-provider/say", String.class);
    }

    // 推荐
    @GetMapping("/say2")
    public String sayHello2() {
        return consumerService.say();
    }
}
